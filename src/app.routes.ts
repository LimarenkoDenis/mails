import { provideRouter, RouterConfig }  from '@angular/router';

import { DevelopersListComponent } from './components/developers';

import { ProjectboxComponent } from './components/projectbox/projectbox.component';
import { ProjectsListComponent } from './components/projectbox/projects/projects-list/projects-list.component';
import { ProjectDetailComponent } from './components/projectbox/projects/project-detail/project-detail.component';

import { CustomersComponent } from './components/customers/customers.component';
import { CustomersListComponent } from './components/customers/customers-list/customers-list.component';
import { CustomerDetailComponent } from './components/customers/customer-detail/customer-detail.component';

const routes: RouterConfig = [
  {
    path: 'developers',
    component: DevelopersListComponent
  },
  {
    path: '',
    redirectTo: 'projects',
    pathMatch: 'full'
  },
  { path: 'projects', component: ProjectboxComponent, children: [
    { path: '',  redirectTo: 'list' },
    { path: 'list',  component: ProjectsListComponent },
    { path: ':id', component: ProjectDetailComponent }
    ]
  },
  { path: 'customers', component: CustomersComponent, children: [
    { path: '',  redirectTo: 'list' },
    { path: 'list',  component: CustomersListComponent },
    { path: ':id', component: CustomerDetailComponent }
    ]
  }
];

export const appRouterProviders = [
  provideRouter(routes)
];
