import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import * as _ from 'lodash';

@Injectable()
export class DevelopersService {
  constructor (private http: Http) {}
  list () {
    return this.http.get(`${process.env.API}/developers`)
                    .map(res => res.json())
  }
}
