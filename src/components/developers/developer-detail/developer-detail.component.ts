import { Component, Input } from '@angular/core';

@Component({
  selector: 'developer-detail',
  template: require('./developer-detail.html')
})

export class DeveloperDetailComponent {
  @Input()
  developer: any;
}
