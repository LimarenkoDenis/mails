import {
    Component,
    Input,
    trigger,
    state,
    style,
    transition,
    animate
} from '@angular/core';
import { DevelopersService } from '../developers.service';
import { DeveloperDetailComponent } from './../developer-detail/developer-detail.component';

@Component({
    moduleId: module.id,
    selector: 'developers-list',
    template: require('./developers-list.html'),
    providers: [DevelopersService],
    directives: [DeveloperDetailComponent],
    animations: [
        trigger('devState', [
            state('in', style({ transform: 'translateX(0)' })),
            transition('void => *', [
                style({ transform: 'translateX(-100%)' }),
                animate(200)
            ]),
            transition('* => void', [
                animate(100, style({ transform: 'translateX(100%)' }))
            ])
        ])
    ]
})

export class DevelopersListComponent {
    developers: any;
    count: number;
    selectedDev: any;
    constructor(private DevelopersService: DevelopersService) { }

    list() {
        this.DevelopersService.list()
            .subscribe(
            (developers: any) => {
                this.count = developers.length;
                this.developers = developers
            });
    }

    ngOnInit() {
        this.list();
    }

    onSelect(developer: any) {
        this.selectedDev = developer;
    }
}
