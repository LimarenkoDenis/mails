import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import * as _ from 'lodash';

@Injectable()
export class ProjectsService {
  constructor (private http: Http) {}
  list () {
    return this.http.get(`${process.env.API}/projects`)
                    .map(res => res.json())
  }
  detail (id:string) {
    return this.http.get(`${process.env.API}/projects/${id}`)
                    .map(res => res.json())
  }
}
