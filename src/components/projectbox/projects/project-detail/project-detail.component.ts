import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProjectsService } from '../projects.service';

@Component({
  selector: 'project-detail',
  template: require('./project-detail.component.html'),
  providers: [ProjectsService]
})

export class ProjectDetailComponent {
  project = {};
  private sub: any;
  constructor(private ProjectsService: ProjectsService, private route: ActivatedRoute){}

  detail(id:string) {
    this.ProjectsService.detail(id)
      .subscribe(
      (project:any) => {
        this.project = project;
       });
  }

  ngOnInit(){
    this.sub = this.route
      .params
      .subscribe( params => {
        let id = params['id'];
        this.detail(id);
      });
  }
}
