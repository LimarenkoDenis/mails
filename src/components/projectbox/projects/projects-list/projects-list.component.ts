import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES, Router } from '@angular/router';
import { ProjectsService } from '../projects.service';

@Component({
  selector: 'projects-list',
  template: require('./projects-list.component.html'),
  directives: [ROUTER_DIRECTIVES],
  providers: [ProjectsService]
})

export class ProjectsListComponent {
  projects:any;
  count:number;
  constructor(private ProjectsService: ProjectsService,  private router: Router) { }

  list() {
    this.ProjectsService.list()
      .subscribe(
      (projects:any) => {
        this.count = projects.length;
        this.projects = projects
       });
  }

  ngOnInit() {
    this.list();
  }

}
