import { Component } from '@angular/core';
import { Router, ROUTER_DIRECTIVES } from '@angular/router';

@Component({
  selector: 'projectbox',
  template: require('./projectbox.component.html'),
  directives: [ROUTER_DIRECTIVES]
})

export class ProjectboxComponent { }
