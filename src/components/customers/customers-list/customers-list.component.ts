import { Component } from '@angular/core';
import { CustomersService } from '../customers.service';
import { CustomerComponent } from './../customer/customer.component';
import { ROUTER_DIRECTIVES, Router } from '@angular/router';

@Component({
    selector: 'customers-list',
    template: require('./customers-list.html'),
    providers: [CustomersService],
    directives: [CustomerComponent, ROUTER_DIRECTIVES]
})

export class CustomersListComponent {
    customers: any;
    count: number;
    constructor(private CustomersService: CustomersService) { }

    list() {
        this.CustomersService.list()
            .subscribe(
            (customers: any) => {
                this.count = customers.length;
                this.customers = customers
            });
    }

    ngOnInit() {
        this.list();
    }

}
