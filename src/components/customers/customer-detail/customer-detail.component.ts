import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomersService } from '../customers.service';

@Component({
  selector: 'customer-detail',
  template: require('./customer-detail.html'),
  providers: [CustomersService]
})

export class CustomerDetailComponent {
  customer = {};
  private sub: any;
  constructor(private CustomersService: CustomersService, private route: ActivatedRoute){}

  detail(id:string) {
    this.CustomersService.detail(id)
      .subscribe(
      (customer:any) => {
        this.customer = customer;
       });
  }

  ngOnInit(){
    this.sub = this.route
      .params
      .subscribe( params => {
        let id = params['id'];
        this.detail(id);
      });
  }
}
