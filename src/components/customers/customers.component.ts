import { Component } from '@angular/core';
import { Router, ROUTER_DIRECTIVES } from '@angular/router';

@Component({
  selector: 'customers',
  template: require('./customers.html'),
  directives: [ROUTER_DIRECTIVES]
})

export class CustomersComponent { }
